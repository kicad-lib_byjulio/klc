---
title: G3 Special Pin Numbering
---

For some classes of devices, the KiCad library uses a pin numbering that is different from the manufacturers' numbering.  The goal is to be able to use generic symbols (and their derived symbols) for a huge number of parts from different manufacturers.
