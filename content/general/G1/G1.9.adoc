+++
title = "G1.9 Dimensional units"
+++

. When specifying a dimension (e.g. in a footprint name) the __units__ must always be provided:
* `3mm` - millimeters
* `1in` - inches
. When specifying areas or volumes, the coordinate order is `X-Y[-Z]`, which is equivalent to `width-height[-thickness]`.
. When specifying multidimensional units (e.g. `width` x `height` x `thickness`) then the units only need to be appended once
* `3x4x7mm` - 3mm x 4mm x 7mm
. Metric units are preferred, where appropriate
. Linear dimensions should be specified in millimeters
. Angular dimensions should be specified in degrees
. Temperature should be specified in degrees Celsius
